const net = require('net')
const http = require('http')
const WebSocket = require('ws')
const Router = require ('./router.js')
const RequestListener = require ('./http_server.js')
const CRC = require('crc-full').CRC

// cria um servidor http
// const host = '127.0.0.1';
const host = '0.0.0.0';
const httpPort = 8000;
httpServer = http.createServer(RequestListener.RequestListener);
httpServer.listen(httpPort, host, () => {
    console.log(`HttpServer is running on http://${host}:${httpPort}`);
});




// cria o servidor que roteia as mensagens
global.router = new Router.Router()
let curSocket = 0
let socketDict = {}

// router.addRoute (1, 2)
// router.addRoute (2, 1)

const CRC_calc = new CRC("CRC16", 16, 0xC86C, 0x0000, 0x0000, false, false)

const checkCrc = (data) =>{
  const dados = new Uint8Array(data)
  const result = CRC_calc.compute(dados.slice(0,-2))
  // console.log(result, dados.at(-2)*256+dados.at(-1))
  return (result == dados.at(-2)*256+dados.at(-1))
}

const handleConnection = sock =>{
  console.log('pedido de conexão',curSocket, sock.remoteAddress)
  sock.seq = curSocket
  sock.rcvOrder = 0
  curSocket++
  sock.setKeepAlive(true, 500)
 
  sock.on('data', data=>{
    
    if (!checkCrc(data)) {
      console.warn(`Problema no CRC em mensagem recebida de ${sock.remoteAddress}`)
      return
    }
    if (sock.rcvOrder == 0) {
      dados = new Uint8Array(data)
      sock.id = dados[0]
      router.registerSocket (sock, sock.id)
      //  socketDict[sock.id]=sock
      sock.rcvOrder ++
      }
    // console.log(`Recebido de id ${sock.id}: ${new Uint8Array(data)}`)
    router.incomingPacket(sock.id, data.slice(0,18))
    response = router.outgoingPacket(sock.id)
    if (response) {
      // console.log(`response to ${sock.id}: ${new Uint8Array(response)}`)
      sock.write(response)
    }
  })
  
  sock.on('end', ()=>{
    // router.deregisterSocket(sock, sock.id)
    console.log('finalizou', sock.id)
  })

  sock.on('close', (event)=>{
    router.deregisterSocket(sock, sock.id)
    console.log('close')
  })

  sock.on('error', (event)=>{
    console.log(`Erro no socket ${sock.id}`)
    console.log(event)
  })

}


port = 3000
const server = net.createServer(handleConnection)
server.on('error', (event)=>{
    console.log('Erro no servidor de socket')
    console.log(event)
})
server.listen(3000,'0.0.0.0', () => {  // ouve em todas as portas no ubuntu
// server.listen(3000,'127.0.0.1', () => {  // para teste usando cliente python
//  server.listen(26, () => {   //  para node no windows
    console.log(`RoutingServer is running on http://${host}:${port}`);
})

// cria um servidor de websocket
const wss = new WebSocket.Server({ port: 7000 });
wss.on('connection', (ws) => {
    console.log('Websocket conectado')
    router.registerWebInterface(ws)
    
    ws.on('end', ()=> {
      router.deregisterWebInterface()
      console.log('Websocket desconectado')})

    ws.on('close', (event)=>{
      console.log('Websocket closed')
      console.log(event)
    })
})

// cria um servidor de websocket só para fazer telnet pelo browser
const configSocket = new WebSocket.Server({ port: 7001 });
configSocket.on('connection', (ws,req) => {
    console.log('Tentativa de conexão para telnet')
    
    const urlParams = new URLSearchParams(req.url.split('?')[1])
    const arduinoId = new Number(urlParams.get('id'))
    
    const connectedArduinos = router.getConnected()
    const arduinoIP = connectedArduinos[arduinoId]
    console.log(`Tentando conectar por telnet com id ${arduinoId} no IP ${arduinoIP}`)

    const telnetConnectionHandler = function (stream) {
      console.log(`Dispositivo com id ${arduinoId} conectado via telnet.`)
      const message = {type: 'server', message:`Dispositivo com id ${arduinoId} conectado via telnet.`}
      ws.send(JSON.stringify(message))
    }
    
    const telnetSocket = net.createConnection({ host: arduinoIP, port: 23 }, telnetConnectionHandler);
    telnetSocket.on('data', (data)=>{
      console.log(data.toString())
      const message = {type: 'remote', message:data.toString()}
      ws.send(JSON.stringify(message))
    })

    telnetSocket.on('end', ()=> {
      console.log(`Telnet desconectado (id ${arduinoId})`)
      const message = {type: 'server', message:`Telnet desconectado.`}
      ws.send(JSON.stringify(message))
    })

    telnetSocket.on('close', (event)=>{
      console.log(`Telnet fechada (id ${arduinoId})`)
      const message = {type: 'server', message:`Telnet fechado.`}
      ws.send(JSON.stringify(message))
      ws.close()
      console.log(event)
    })

    telnetSocket.on('error', (event)=>{
      console.log(`Erro no telnet (id ${arduinoId})`)
      console.log(event)
    })

    ws.on('message', (message)=>{
      telnetSocket.write(message)

    })
    
    ws.on('end', ()=> {
      console.log(`Websocket para telnet desconectado (id ${arduinoId})`)

    })

    ws.on('close', (event)=>{
      console.log(`Websocket para telnet fechada (id ${arduinoId})`)
      telnetSocket.write('quit\n')
      telnetSocket.end()
      console.log(event)
    })
})

