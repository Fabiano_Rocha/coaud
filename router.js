const fs = require('fs')

function Router() {
  this.socketList = {}
  this.routes = {}
  this.lastPacket = {}
  this.nextPacket = {}
  this.idDescription = {}
  this.webInterface = undefined

  this.registerWebInterface = function (ws) {
    this.webInterface = ws
  }

  this.deregisterWebInterface = function () {
    this.webInterface = undefined
  }
  
  this.registerSocket = function (sock, id) {
    this.socketList[id] = sock
    this.lastPacket[id] = null
    this.log ('connection', id, null)
  }

  this.deregisterSocket = function (sock, id) {
    delete this.socketList[id]
    delete this.lastPacket[id]
    delete this.nextPacket[id]
    this.log ('disconnection', id, null)
  }

  this.incomingPacket = function (id, data) {
    try {
      if (Buffer.compare(data, this.lastPacket[id]) != 0) {
        this.log('change',id, data)
      }
    } catch (err) {
      if (err.code != 'ERR_INVALID_ARG_TYPE') {
        throw err
      } else {
        this.log('change',id, data) // first send
      }
    }
    this.lastPacket[id] = data
    this.redirectPacket (id, data)
  }

  this.outgoingPacket = function (id) {
    try {
      resp = this.nextPacket[id]
      this.nextPacket[id] = null
      return resp
    } catch (err) {
      return null
    }
  }
  
  this.redirectPacket = function (id, data) {
    try {
      destinations = this.routes[id]
      for (dest of destinations){
        this.nextPacket[dest] = data
      }
    } catch (err){
      // console.log(`No route for source ${id}.`)
      // throw err
    }
    // console.log(`next packet to ${dest}: ${new Uint8Array(this.nextPacket[dest])} 
    // (${new Uint8Array(data)})`)
  }

  this.addRoute = function (id_source, id_destination) {
    let routes
    if (typeof(id_destination) == 'string'){
      routes = id_destination.split(',').map(Number)
    } else if (typeof(id_destination) == 'number'){
      routes = [id_destination]
    } else {
      console.log('wrong type on parameter for router.addRoute')
      return
    }

    this.routes[id_source] = routes
    this.saveRoutes()
    this.log('addRoute', id_source, id_destination)
  }

  this.eraseRoute = function (id_source) {
    delete this.routes[id_source]
    this.saveRoutes()
  }

  this.saveRoutes = function () {
    table = JSON.stringify(this.routes,null, '\t')
    fs.writeFile('routes.txt', table, err => {
      if (err) {
        console.log(err)
      }
    })
  }

  this.loadRoutes = function () {
    try {
      data = fs.readFileSync('routes.txt', 'utf-8')
      return JSON.parse(data)
    } catch (err){
      if (err.code=='ENOENT') {
        console.log(err)
        return {}
      }
    }

  }

  this.addDescription = function (id, desc) {
    this.idDescription[id] = desc
    this.saveDescriptions()
    this.log('addDescription', id, desc)
  }

  this.saveDescriptions = function () {
    table = JSON.stringify(this.idDescription,null, '\t')
    fs.writeFile('descriptions.txt', table, err => {
      if (err) {
        console.log(err)
      }
    })
  }

  this.loadDescriptions = function () {
    try {
      data = fs.readFileSync('descriptions.txt', 'utf-8')
      return JSON.parse(data)
    } catch (err){
      if (err.code=='ENOENT') {
        console.log(err)
        return {}
      }
    }

  }
  
  this.eraseDescription = function (id) {
    delete this.idDescription[id]
    this.saveDescriptions()
  }

  this.getRoutes = function () {
    return JSON.stringify(this.routes,null, '\t')
  }

  this.getDescriptions = function() {
    return JSON.stringify(this.idDescription, null, '\t')
  }

  this.getConnected = function() {
    var connected = {};
    for (const key in this.socketList){
      connected[key] = this.socketList[key].remoteAddress;
    }
    // return JSON.stringify(connected, null, '\t')
    return connected
  }

  this.getRegistered = function() {
    var registered = {};
    for (const key in this.idDescription){
      registered[key]= {'id': key}
      try {
        registered[key]['ip'] = this.socketList[key].remoteAddress
      } catch (error) {
        registered[key]['ip'] = "";
        // console.log(`${error}`)
      }
      registered[key]['description'] = this.idDescription[key]
      
      try {
        registered[key]['route'] = this.routes[key]
      } catch (error) {
        registered[key]['route'] = "";
        // console.log(`${error}`)
      }
      
      try {
        registered[key]['message'] = `${new Uint8Array(this.lastPacket[key])}`
      } catch (error) {
        registered[key]['message'] = "";
        // console.log(`${error}`)
      }
      
      
    }
    return JSON.stringify(registered, null, '\t')
  }

  this.getUnregistered = function(){
    let unregs = []
    try{
      connIds = Object.keys(this.socketList)
    }catch(error){
      if (error.code == 'TypeError'){
        connIds = []
      }
    }
    try{
      descIds = Object.keys(this.idDescription)
    } catch(error){
      descIds = []
    }
    console.log(connIds)
    console.log(descIds)
    try{
        connIds.forEach((connected)=>{
          if (!descIds.includes(connected)){
              unregs.push(connected)
          }
        })
    } catch (error){
      console.log(error)
    }
    return JSON.stringify(unregs, null, '\t')
  }

  // this.getDisconnected = function(){

  //   let registered = this.idDescription.keys()
  //   registered.forEach(function(idReg) {
  //     if (this.connected == undefined) {
        
  //     } else {
        
  //     }
  //   })
  // }

  this.log = function (event, id, data) {
    if (event=='change'){
      dados = new Uint8Array(data)
      console.log(`Id ${id} changed input to ${dados}`)
    }
    //console.warn(`log ${event} not implemented`)
    this.updateWebInterface(event, id, data)
  }

  this.updateWebInterface = function(event, id, data){
    let dados = {}
    if (this.webInterface == undefined){
      return
    }
    switch (event){
      case 'change':
        d = `${new Uint8Array(data)}`
        dados[id] = {'message': d}
        break
      case 'connection':
        dados[id] = {'ip': this.socketList[id].remoteAddress}
        break
      case 'addRoute':
        dados[id] = {'route': this.routes[id]}
      case 'disconnection':
        dados[id] = {'ip': "",
                     'message':''}
        break
      case 'addDescription':
        dados[id] = {'registered': id, 
                     'ip': this.routes[id],
                     'description': this.idDescription[id],
                     'route': this.routes[id],
                     'message': `${new Uint8Array(this.lastPacket[id])}`}
        break
      default:
        return
      
    }
    try {
        this.webInterface.send(JSON.stringify(dados))
    } catch (error){
      if(!(error instanceof TypeError)) {
        console.log(error)
      }
    }
  }

  this.routes = this.loadRoutes()
  this.idDescription = this.loadDescriptions()

}

module.exports = {Router}
