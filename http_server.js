const fs = require('fs/promises')
const qs = require('querystring')

async function RequestListener (req, res) {
  console.log(`tentativa de conexão http ${req.method} ${req.url}`)
  console.log(new URLSearchParams(req.url))
  if (req.method=='GET'){
    const url = new URL(req.url, `http://${req.headers.host}`)
    switch (url.pathname){
      case "/routes":
        res.setHeader("Content-Type", "application/json");
        res.writeHead(200);
        res.end(router.getRoutes());
        break;
      case "/connected":
        res.setHeader("Content-Type", "application/json");
        res.writeHead(200);
        res.end(JSON.stringify(router.getConnected(),null, '\t'));
        break;
      case "/registered":
        res.setHeader("Content-Type", "application/json");
        res.writeHead(200);
        res.end(router.getRegistered());
        break;
      case "/unregistered":
        res.setHeader("Content-Type", "application/json");
        res.writeHead(200);
        res.end(router.getUnregistered());
        break;
      case "/config":
        console.log('asked for config.html')
        const config = await fs.readFile('config2.html')
        res.setHeader("Content-Type", "text/html");
        res.writeHead(200);
        res.end(config);
        break;
      case "/":
        console.log('asked for index.html')
        const index = await fs.readFile('index.html')
        res.setHeader("Content-Type", "text/html");
        res.writeHead(200);
        res.end(index);
        break
      case "/styles.css":
        const css = await fs.readFile('styles.css')
        console.log('ready to send css')
        res.setHeader("Content-Type", "text/css");
        res.writeHead(200);
        res.end(css);
        break
      case "/frontendUtils.js":
        const utils = await fs.readFile('frontendUtils.js')
        console.log('ready to send js')
        res.setHeader("Content-Type", "text/javascript");
        res.writeHead(200);
        res.end(utils);
        break
      case "/Icons/edit.png":
        let img = await fs.readFile('./Icons/edit.png')
        console.log('ready to send image')
        res.setHeader("Content-Type", "image/png");
        res.writeHead(200);
        res.end(img);
        break
      case "/favicon.ico":
        let icon = await fs.readFile('./Icons/favicon.ico')
        console.log('ready to send image')
        res.setHeader("Content-Type", "image/vnd.microsoft.icon");
        res.writeHead(200);
        res.end(icon);
        break
      default:
        console.log(`Didn't find ${req.path}`)
        res.writeHead(400);
        res.end();
    }
  }
  else if (req.method=='POST'){
    console.log(`Post request`)
    let rawData = ''
    switch (req.url){
      case "/addNewDevice":
        req.on('data', function(data) {
          rawData += data
        })

        req.on('end', function() {
          let {newDevice, newDescription} = qs.decode(rawData)
          console.log(rawData)
          console.log(`Post request add description ${newDescription} to ${newDevice}`)
          router.addDescription(newDevice, newDescription)
          res.end()
        })
        break;
      case "/addRoute":
        req.on('data', function(data) {
          rawData += data
        })

        req.on('end', function() {
          let {Id, routes} = qs.decode(rawData)
          console.log(rawData)
          console.log(`Post request add routes ${routes} to ${ID}. `)
          router.addRoute(Id, routes)
          res.end()
        })
        break
      default:
        res.end()
        
    }
  }

  return
};

module.exports = {RequestListener}
