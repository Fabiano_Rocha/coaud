function validate(input) {
if (input.value.length == 0) {
    input.style.background = 'lightcoral';
    aviso = document.getElementById('aviso')
    aviso.textContent = 'Obrigatório'
    // console.log(input.style)
    return false
  }
else {
    aviso = document.getElementById('aviso')
    aviso.innerHTML = ''
    input.style.background = '';
    return true
  }  
}

function formSubmit(id, desc){
        console.log(id.value)
        console.log(desc.value)

    if (validate(desc)){
        params = `newDevice=${id.value}&newDescription=${desc.value}`
        fetch("/addNewDevice",  
              {method:'post', 
                body: params}
             ).then(()=>{
                 populateTable('registeredTable')
                 updateUnregisteredIds(document.getElementById('newDevSelect'))
             }
                )
    }
    return false
}

// function submitDesc(ok) {
//     if (ok){
//         res = document.formNewDevice.submit()
//         console.log(res)
//     }
//     return false
// }


async function updateUnregisteredIds(input) {
    idList = await fetchServer('unregistered')
    // console.log(idList)
    // console.log(input)
    input.innerHTML = ''  // clear select
    textInput = document.getElementById('newDescription')
    textInput.value = '' //clear text input

    idList.forEach((id, key)=>{
        input[key] = new Option(id, id)
    })

}

function updateSelect(data){
    for (item of Object.keys(data)) {
        if (!(item in tableObjs)){ // if update unregistered ids
            updateUnregisteredIds(document.getElementById('newDevSelect'))
            break
        }
    }
}

function selectedNewDevice(input) {
    let id = input.value
    console.log(`Selecionou id ${input}`)
    document.getElementById('newDescription').value=id
}

async function fetchServer(url) {
    // console.log(url)
    let response = await fetch(url)
    return response.json()
}

async function populateTable(tableId) {
    let columns = ['id', 'ip', 'description', 'message', 'route']
    let data = await fetchServer('registered')
    // console.log('populateTable')
    let table = document.getElementById(tableId);
    drawTable(table, columns, data)
}


let tableObjs = {}

function drawTable(table, columns, data){
    // table will be ordered by primary keys on data object
    let items = Object.keys(data)
    items.map(i => {return parseInt(i)}) //convert to int
    items.sort()

    // clear table
    table.replaceChildren();

    items.forEach( item => {
        let newRow = table.insertRow()
        tableObjs[item] = {'row': newRow}
        columns.forEach (col =>{
            let newCell = newRow.insertCell()
            newCell.className = 'cell'
            if (data[item][col]==undefined){
                newCell.innerHTML = ''
            } else {
                newCell.innerHTML = data[item][col]
            }
            tableObjs[item][col] = newCell
        })
    })
    // console.log(tableObjs)
    addButtonsToRows()
}

function addButtonsToRows(){
    let items = Object.keys(tableObjs)
    items.forEach(item =>{
        var row = tableObjs[item]['row']
        var newCell = row.insertCell()

        var img = document.createElement('img')
        img.src = '/Icons/edit.png'
        img.width = 25
        
        var link = document.createElement('a')
        link.appendChild(img)
        link.title = 'Configurações'
        link.href = `/config?id=${item}`
        link.setAttribute('target', '_blank')
        newCell.appendChild(link)

    })
}

// let teste = 'a'
function updateTable(data){
    // let data1 = { '1': {'message': teste}}
    // data1['2'] = {'ip': teste}
    // data = data1
    // console.log(data)

    for (item of Object.keys(data)) {
        // console.log(item)
        if ('registered' in Object.keys(data)) {
            populateTable('registeredTable')
            return
        }
        if (item in tableObjs){
            for (col of Object.keys(data[item])){
                // console.log(col)
                if (data[item][col]==undefined){
                    tableObjs[item][col].innerHTML = ''
                } else {
                    tableObjs[item][col].innerHTML = data[item][col]
                }
            }   
        }
    }
    // teste += 'a'
}

function connectToServer(){
    let socket = new WebSocket('ws://'+window.location.hostname+':7000')
    socket.onopen = function(e) {
        console.log("[open] Connection established");
        writeOnFooter('on','Servidor conectado.')
    };

    socket.onmessage = function(event) {
        // console.log(event)
        updateTable(JSON.parse(event.data));
        updateSelect(JSON.parse(event.data));
    };

    socket.onclose = function(event) {
        if (event.wasClean) {
            console.log(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        
        } else {
            // e.g. server process killed or network down
            // event.code is usually 1006 in this case
             console.log('[close] Websocket connection died');
             console.log('event')
        }
        writeOnFooter('off','Servidor desconectado.')
    };

    socket.onerror = function(error) {
        alert(`[error] ${error.message}`);
    };
}

async function initialize(){
    await populateTable('registeredTable')
    await updateUnregisteredIds(document.getElementById('newDevSelect'))
    connectToServer()
}

function writeOnFooter (code, message) {
    foot = document.getElementById('footerMessage')
    foot.innerHTML=message
    // switch (code){
    //     case 'on':
    //         foot.classList.add('ok');
    //         break;
    //     case 'off':
    //         foot.classList.remove('ok');
    //         break;
    // }
}